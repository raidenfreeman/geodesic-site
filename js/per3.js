function RadiansToGradians(radians) {
    return radians * 200 / Math.PI;
}

function GradiansToRadians(gradians) {
    return gradians * Math.PI / 200;
}

//expecting number like X.XXXXX....
function RoundHalfToEven(num, decimalPlaces) {
    var d = decimalPlaces || 0;
    var m = Math.pow(10, d);
    var n = +(d ? num * m : num).toFixed(8); // Avoid rounding errors
    var i = Math.floor(n), f = n - i;
    var e = 1e-8; // Allow for rounding errors in f
    var r = (f > 0.5 - e && f < 0.5 + e) ?
        ((i % 2 == 0) ? i : i + 1) : Math.round(n);
    return d ? r / m : r;
}

function RoundUp(num, decimalPlaces) {
    var m = Math.pow(10, decimalPlaces);
    return Math.round(num * m) / m;
}


function CuttofAtDecimal(num, decimalPlaces) {
    var m = Math.pow(10, decimalPlaces);
    return Math.floor(num * m) / m;
}

console.assert(RoundHalfToEven(0.00136, 4) == 0.0014, "Round error");
console.assert(RoundHalfToEven(0.00134, 4) == 0.0013, "Round error");
console.assert(RoundHalfToEven(4000000.00135, 4) == 4000000.0014, "Round error");
console.assert(RoundHalfToEven(0.00125, 4) == 0.0012, "Round error");
console.assert(RoundUp(0.00125, 4) == 0.0013, "Round error");
console.assert(RoundUp(0.00135, 4) == 0.0014, "Round error");
console.assert(RoundUp(0.00131, 4) == 0.0013, "Round error");
console.assert(RoundUp(0.00136, 4) == 0.0014, "Round error");
console.assert(RoundUp(0.00123, 4) == 0.0012, "Round error");
console.assert(RoundUp(0.00126, 4) == 0.0013, "Round error");

function handleFileSelect(evt) {
    var files = evt.target.files;
    var i, f;
    for (i = 0, f = files[i]; i != files.length; ++i) {
        var reader = new FileReader();
        var name = f.name;
        //console.log(name);
        reader.onload = function (evt) {

            var xT1, xS1, xSn1, xT2, yT1, yS1, ySn1, yT2;

            const orizontiesGonies = [];
            const katakorifesGonies = [];
            const miki = [];

            var mesoIpsos = undefined;
            var sintelestis = undefined;

            var data = evt.target.result;

            var workbook = XLSX.read(data, {type: 'binary'});
            var sheet_name_list = workbook.SheetNames;
            sheet_name_list.forEach(function (y) { /* iterate through sheets */
                var worksheet = workbook.Sheets[y];
                for (var z in worksheet) {
                    /* all keys that do not begin with "!" correspond to cell addresses */
                    if (z[0] === '!') continue;
                    if (z[0] === 'A' && z[1] != '1') {
                        orizontiesGonies[parseInt(z[1], 10) - 2] = worksheet[z].v;
                    }
                    if (z[0] === 'B' && z[1] != '1') {
                        katakorifesGonies[parseInt(z[1], 10) - 2] = worksheet[z].v;
                    }
                    if (z[0] === 'C' && z[1] != '1') {
                        miki[parseInt(z[1], 10) - 2] = worksheet[z].v;
                    }

                    //if we are on column D
                    if (z[0] === 'D') {
                        if (z[1] === '2')
                            xT1 = worksheet[z].v;
                        if (z[1] === '3')
                            xS1 = worksheet[z].v;
                    }
                    //if we are on column E
                    if (z[0] === 'E' && z[1] != '1') {
                        if (z[1] === '2')
                            yT1 = worksheet[z].v;
                        if (z[1] === '3')
                            yS1 = worksheet[z].v;
                    }

                    if (z[0] === 'F' && z[1] != '1') {
                        if (z[1] === '2')
                            mesoIpsos = worksheet[z].v;
                    }
                    if (z[0] === 'G' && z[1] != '1') {
                        if (z[1] === '2')
                            sintelestis = worksheet[z].v;
                    }
                }
                if (katakorifesGonies.length != miki.length)
                    console.error("oi katakorifes gonies prepei na einai ises me ta miki");
                if (katakorifesGonies.length != orizontiesGonies.length)
                    console.error("oi katakorifes gonies prepei na einai ises me tis orizonties");
            });

            var aT1S1 = IpologismosArxikisGoniasDieuthinsis(xT1, xS1, yT1, yS1);

            var plithosOriziontionGonion = orizontiesGonies.length;
            var plithosKatakorifonGonion = katakorifesGonies.length;

            console.log("aT1S1  " + aT1S1);

            var i;

            var aArray = [];

            for (i = 0; i < plithosOriziontionGonion; i++) {
                if (i === 0) {
                    aArray[i] = RoundHalfToEven(NormalizeToGrad(aT1S1 + orizontiesGonies[i] + 200), 4);
                }
                else
                    aArray[i] = RoundHalfToEven(NormalizeToGrad(aArray[i - 1] + orizontiesGonies[i] + 200), 4);
            }

            console.log(aArray);

            var orizontioMikos = [];
            for (i = 0; i < plithosKatakorifonGonion; i++) {
                orizontioMikos[i] = RoundUp(Math.sin(GradiansToRadians(katakorifesGonies[i])) * miki[i], 3);
                if (mesoIpsos != undefined && sintelestis != undefined) {
                    console.log("boom");
                    orizontioMikos[i] = orizontioMikos[i] * sintelestis * 6371000 / (6371000 + mesoIpsos);
                }
            }
            console.log(orizontioMikos);

            var delta1x = [];
            for (i = 0; i < plithosKatakorifonGonion; i++) {
                delta1x[i] = RoundUp(orizontioMikos[i] * Math.sin(GradiansToRadians(aArray[i])), 3);
            }
            console.log(delta1x);

            var delta1y = [];
            for (i = 0; i < plithosKatakorifonGonion; i++) {
                delta1y[i] = RoundUp(orizontioMikos[i] * Math.cos(GradiansToRadians(aArray[i])), 3);
            }
            console.log(delta1y);

            var xS = [];
            xS[0] = xS1 + delta1x[0];
            for (i = 1; i < plithosKatakorifonGonion; i++) {
                xS[i] = xS[i - 1] + delta1x[i];
            }

            var yS = [];
            yS[0] = yS1 + delta1y[0];
            for (i = 1; i < plithosKatakorifonGonion; i++) {
                yS[i] = yS[i - 1] + delta1y[i];
            }

            console.log(xS);
            console.log(yS);

            (function () {
                var tableBody = document.getElementById("resultTableBody");
                while (tableBody.firstChild) {
                    tableBody.removeChild(tableBody.firstChild);
                }
                var tr = document.createElement('TR');
                tableBody.appendChild(tr);
                var th1 = document.createElement('TH');
                th1.appendChild(document.createTextNode("Σημείο"));
                tr.appendChild(th1);
                var th2 = document.createElement('TH');
                th2.appendChild(document.createTextNode("x(m)"));
                tr.appendChild(th2);
                var th3 = document.createElement('TH');
                th3.appendChild(document.createTextNode("y(m)"));
                tr.appendChild(th3);


                for (i = 0; i < plithosKatakorifonGonion; i++) {
                    var tr = document.createElement('TR');
                    var td1 = document.createElement('TD');
                    td1.appendChild(document.createTextNode("Σ" + (i + 2)));
                    tr.appendChild(td1);
                    var td2 = document.createElement('TD');
                    td2.appendChild(document.createTextNode(xS[i].toFixed(3)));
                    tr.appendChild(td2);
                    var td3 = document.createElement('TD');
                    td3.appendChild(document.createTextNode(yS[i].toFixed(3)));
                    tr.appendChild(td3);
                    tableBody.appendChild(tr);
                }
            })();

        };

        reader.readAsBinaryString(f);
    }
// files is a FileList of File objects. List some properties.
    /*var output = [];
     for (var i = 0, f; f = files[i]; i++) {
     output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
     f.size, ' bytes, last modified: ',
     f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
     '</li>');
     }
     document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';*/
}

function NormalizeToGrad(grad) {
    while (grad >= 400) {
        grad = grad - 400;
    }
    while (grad < 0) {
        grad = grad + 400;
    }
    return grad;
}

function IpologismosArxikisGoniasDieuthinsis(a, b, c, d) {
    var apotelesma;
    var arithmitis = (b - a);
    var paronomastis = (d - c);
    if (paronomastis === 0) {
        if (arithmitis > 0)
            return apotelesma = 100;
        if (arithmitis < 0)
            return apotelesma = 300;
    }
    else {
        if (arithmitis === 0) {
            if (paronomastis > 0)
                return apotelesma = 0;
            if (paronomastis < 0)
                return apotelesma = 200;
        }
        else {
            var atan = Math.atan(arithmitis / paronomastis);
            apotelesma = RadiansToGradians(atan);
            apotelesma = RoundHalfToEven(apotelesma, 4);
            //console.assert(apotelesma === -9.9490, "apotelesma is wrong!");
            //console.log(apotelesma);

            if (arithmitis > 0 && paronomastis < 0 || arithmitis < 0 && paronomastis < 0) {
                apotelesma = apotelesma + 200;
            }
            if (arithmitis < 0 && paronomastis > 0) {
                apotelesma = apotelesma + 400;
            }
        }
    }
    return NormalizeToGrad(apotelesma);
}

function SumArray(array) {
    return array.reduce(function (a, b) {
        return a + b;
    });
}

document.getElementById('files').addEventListener('change', handleFileSelect, false);